from django.apps import AppConfig


class Modul2Config(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'modul2'
