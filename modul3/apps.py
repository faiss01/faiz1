from django.apps import AppConfig


class Modul3Config(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'modul3'
